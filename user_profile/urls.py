from django.urls import path
from . import views
import donation.views as donation_views

app_name = "user_profile"

urlpatterns = [
    path('', views.user_profile, name="user_profile"),
]