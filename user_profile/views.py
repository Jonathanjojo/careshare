from django.shortcuts import render, redirect
from django.http import HttpResponse
from regisform.models import Donor

# Create your views here.
def user_profile(request):
    if 'user_id' in request.session:
        response = {
            "navs"      : True,
            "footer"    : "CareShare, est. 2018",
            "donations" : Donor.objects.get(email=request.session['email']).donations.all
        }
        return render(request, "user_profile/profile.html", response)
    else:
        return redirect("news:news_page")