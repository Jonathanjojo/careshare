from django.shortcuts import render, HttpResponse

def landing_page(request):
    response = {
        "page_title"    : "Welcome to CareShare!",
        "navs"          : {},
        "footer"        : None,
    }
    return render(request, 'landing_page/landing_page.html', response)