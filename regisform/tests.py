from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.apps import apps
from .apps import RegisformConfig
from .models import Donor

class RegisFormAppTest(TestCase):
	def test_apps_config(self):
		self.assertEqual(RegisformConfig.name, 'regisform')
		self.assertEqual(apps.get_app_config('regisform').name, 'regisform')

class DonorModelTest(TestCase):
	def test_str_representation(self):
		donor = Donor(username="Donor Name")
		self.assertEqual(str(donor), donor.username)
