from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.
class Donor(models.Model):
    username = models.CharField(max_length = 30)
    email = models.CharField(max_length = 30)

    def __str__(self):
        return self.username

    class Meta:
        verbose_name_plural = "Donors"