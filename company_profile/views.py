from django.shortcuts import render
from .models import Testimony
from .forms import TestimonyForm
from django.http import JsonResponse
from regisform.models import Donor
import json

def profile(request):
    response = {
        'testimonies' : Testimony.objects.all()[::-1],
        'navs'        : True,
        'footer'      : "CareShare, est.2018"
    }
    if request.method == "POST":
        data = json.loads(request.body)
        form = TestimonyForm({'testimony':data['testimony']})
        if form.is_valid():
            submiter = Donor.objects.get(email=request.session['email'])
            Testimony.objects.create(
                testimony = form.cleaned_data['testimony'],
                submiter = submiter
            )
            return JsonResponse({
                'testimony' : form.cleaned_data['testimony'],
                'submiter' : submiter.username,
                'status' : 1
            })
        else:
            return JsonResponse({
                'status' : 0
            })
    else: 
        if "user_id" in request.session:
            response['form'] = TestimonyForm()
    return render(request, 'company_profile/about.html', response)