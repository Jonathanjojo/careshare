from django.db import models

# Create your models here.
class Testimony(models.Model):
    testimony = models.CharField(max_length=100)
    submiter = models.ForeignKey('regisform.Donor', on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.testimony + " - " + self.submiter.username

    class Meta:
        verbose_name_plural = "Testimonies"