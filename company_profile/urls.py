from django.urls import path
from . import views
import donation.views as donation_views

app_name = "company_profile"

urlpatterns = [
    path('', views.profile, name="about")
]