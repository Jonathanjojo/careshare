from django.http import JsonResponse, HttpResponse
from django.urls import reverse
from google.oauth2 import id_token
from regisform.models import Donor
import google.auth.transport.requests as grequests
import requests
import json

def login(request):
    if request.method == "POST":
        try:
            token = request.POST['id_token']
            idinfo = id_token.verify_oauth2_token(
                token, 
                grequests.Request(),
                "302110534751-9kjukh2b0rrhk1ssv2o55cgm4kllbm3q.apps.googleusercontent.com"
            )
            if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
                raise ValueError('Wrong issuer.')
            request.session['user_id'] = idinfo['sub']
            request.session['name'] = idinfo['name']
            request.session['email'] = idinfo['email']
            request.session['picture'] = idinfo['picture']
            
            if Donor.objects.filter(email=request.session['email']).first() == None:
                Donor.objects.create(
                    username = request.session['name'],
                    email = request.session['email']
                )
            return JsonResponse({
                "status" : 1,
                "username" : request.session['name'],
                "url" : reverse("news:news_page")
            })
        except (ValueError, KeyError):
            return JsonResponse({
                "status" : 0
            })
    return JsonResponse({
        "status" : 0
    })

def logout(request):
    if request.method == "POST":
        request.session.flush()
        return JsonResponse({
            "url": reverse("landing_page:landing_page")
        })
    else:
        return HttpResponse("Method not Allowed", status=403)

def is_auth(request):
    return JsonResponse({
        "is_auth" : "user_id" in request.session
    })