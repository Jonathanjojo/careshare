from django.test import TestCase
from regisform.models import Donor
import json

class AuthTest(TestCase):
    def test_log_in(self):
        donor = Donor(username="name", email="email@email.com")
        donor.save()
        response = self.client.post('/auth/login/',{"id_token" : "TOKEN"})
        data = response.json()        
        self.assertEqual(data['status'], 0)
        response = self.client.get('/auth/login/')
        self.assertEqual(data['status'], 0)
    
    def test_log_out(self):
        donor = Donor(username="name", email="email@email.com")
        donor.save()
        response = self.client.post('/auth/logout/')
        data = response.json()
        self.assertTrue('url' in data)
        response = self.client.get('/auth/logout/')
        self.assertEqual(response.status_code, 403)

    def test_auth_status(self):
        response = self.client.get('/auth/is_authenticated/')
        data = response.json()
        self.assertTrue('is_auth' in data)