from django.shortcuts import render, HttpResponse
from .models import Program

# Create your views here.
def program_list(request):
    programs = Program.objects.filter(available=True)
    response = {
        "page_title": "Programs Page",
        "programs"  : programs,
        "title"     : "Donation Programs",
        "navs"      : True,
        "footer"    : "CareShare, est.2018"
    }
    return render(request, 'program/program_list.html', response)

def program_details(request, slug):
    program = Program.objects.get(slug=slug)
    response = {
        "page_title": program.name + " Details",
        "program"   : program,
        "title"     : program.name,
        "navs"      : True,
        "footer"    : "CareShare, est.2018"
    }
    return render(request, 'program/program_details.html', response)