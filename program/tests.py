from django.test import TestCase
from django.apps import apps
from django.urls import resolve
from .apps import ProgramConfig
from .models import Program
from regisform.models import Donor
from donation.models import Donation
from .views import (
    program_details, 
    program_list
)

class ProgramAppTest(TestCase):
    def test_apps_config(self):
        self.assertEqual(ProgramConfig.name, 'program')
        self.assertEqual(apps.get_app_config('program').name, 'program')

class ProgramListPage(TestCase):
    def test_program_list_page_url_accessible(self):
        response = self.client.get('/programs/')
        self.assertEqual(response.status_code, 200)

    def test_using_program_list_func(self):
        found = resolve('/programs/')
        self.assertEqual(found.func, program_list)

    def test_program_list_uses_correct_template(self):
        response = self.client.get('/programs/')
        self.assertTemplateUsed(response, 'program/program_list.html')

    def test_program_list_title_message(self):
        response = self.client.get('/programs/')
        self.assertContains(response, 'Donation Programs')

    def test_display_only_available_programs(self):
        program = Program()
        program.name = "Program Title 1"
        program.save()
        program = Program()
        program.name = "Program Title 2"
        program.available = False
        program.save()
        response = self.client.get('/programs/')
        self.assertContains(response, 'Program Title 1')
        self.assertNotContains(response, 'Program Title 2')

class ProgramDetailsPage(TestCase):
    def test_program_details_page_url_accessible(self):
        program = Program()
        program.name = "Program Title"
        program.save()
        response = self.client.get('/programs/program-title/')
        self.assertEqual(response.status_code, 200)

    def test_using_program_details_func(self):
        program = Program()
        program.name = "Program Title"
        program.save()
        found = resolve('/programs/program-title/')
        self.assertEqual(found.func, program_details)

    def test_program_details_uses_correct_template(self):
        program = Program()
        program.name = "Program Title"
        program.save()
        response = self.client.get('/programs/program-title/')
        self.assertTemplateUsed(response, 'program/program_details.html')

class ProgramModelTests(TestCase):
    def test_default_attribute(self):
        program = Program()
        self.assertEqual(program.name, '')
        self.assertEqual(program.desc, '')
        self.assertEqual(program.holder, '')
        self.assertEqual(program.available, True)

    def test_program_string_representation(self):
        program = Program(name="Program Name")
        self.assertEqual(str(program), program.name)

    def test_program_verbose_name_plural(self):
        self.assertEqual(str(Program._meta.verbose_name_plural), "programs")

    def test_snippet_program_desc(self):
        program = Program(desc=501*"X")
        self.assertEqual(program.snipped_desc(), 500*"X" + " ....")

    def test_normal_program_desc(self):
        program = Program(desc=100*"X")
        self.assertEqual(program.snipped_desc(), 100*"X")

    def test_slugify_program_name(self):
        program = Program()
        program.name = "Program Title, Program Subtitle..."
        program.save()
        self.assertEqual(program.slug, "program-title-program-subtitle")
    
    def test_total_donation(self):
        program = Program()
        program.save()
        donor = Donor()
        donor.save()
        donation1 = Donation(program=program, donor=donor, amount=100)
        donation1.save()
        donation2 = Donation(program=program, donor=donor, amount=11)
        donation2.save()
        self.assertEqual(program.total_donation(), 111)