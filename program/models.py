from django.db import models
from news.models import News
from donation.models import Donation
import re

class Program(models.Model):
    name = models.CharField(max_length=100)
    slug = models.CharField(max_length=100)
    desc = models.CharField(max_length=1000)
    holder = models.CharField(max_length=50)
    available = models.BooleanField(default=True)
    news = models.ForeignKey(News, on_delete=models.CASCADE, related_name='programs', blank=True, null=True)
    
    def save(self, *args, **kwargs):
        if not self.slug:
            self.set_slug()
        super(Program, self).save(*args, **kwargs)

    def set_slug(self):
        s = self.name.lower()
        for c in [' ', '-', '.', '/']:
            s = s.replace(c, '_')
        s = re.sub('\W', '', s)
        s = s.replace('_', ' ')
        s = re.sub('\s+', ' ', s)
        s = s.strip()
        s = s.replace(' ', '-')
        self.slug = s

    def __str__(self):
        return self.name

    def snipped_desc(self):
        if len(self.desc) > 500:
            return self.desc[:500] + " ...."
        return self.desc

    def total_donation(self):
        total = 0
        for donation in self.donations.all():
            total += donation.amount
        return total

    class Meta:
        verbose_name_plural = "programs"