from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

# Create your models here.
class Donation(models.Model):
	donor = models.ForeignKey('regisform.Donor', on_delete=models.CASCADE, related_name="donations", blank=True, null=True)
	anon = models.BooleanField(default=False)
	amount = models.IntegerField(
		default = 0, 
		validators=[
			MinValueValidator(1),
			MaxValueValidator(9999999999999)]
	)
	program = models.ForeignKey('program.Program', on_delete=models.CASCADE, related_name="donations", blank=True, null=True)
	
	def __str__(self):
		return self.donor.username + "-" + str(self.amount) + "-" + self.program.name

	class Meta:
		verbose_name_plural = "donations"
