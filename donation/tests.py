from django.apps import apps
from django.test import TestCase
from django.urls import resolve
from .apps import DonationConfig
from .models import Donation
from .forms import DonationForm
from program.models import Program
from regisform.models import Donor
from .views import program_donate
import json

# Create your tests here.
class DonationAppTest(TestCase):
    def test_apps_config(self):
        self.assertEqual(DonationConfig.name, 'donation')
        self.assertEqual(apps.get_app_config('donation').name, 'donation')

class DonationModelTest(TestCase):
    def test_default_attribute(self):
        donation = Donation()
        self.assertEqual(donation.program, None)
        self.assertEqual(donation.donor, None)
        self.assertEqual(donation.amount, 0)
        self.assertEqual(donation.anon, False)

    def test_donation_string_representation(self):
        program = Program(name="PROGRAMNAME")
        donor = Donor(username="USERNAME", email="EMAIL@EMAIL.COM")
        donation = Donation(program=program, anon=False, donor=donor, amount=100)
        self.assertEqual(str(donation), "USERNAME-100-PROGRAMNAME")

    def test_donation_verbose_name_plural(self):
        self.assertEqual(str(Donation._meta.verbose_name_plural), "donations")
    
class DonationViewTestWithSession(TestCase):
    def setUp(self):
        session = self.client.session
        session["user_id"] = "USER_ID"
        session["name"] = "name"
        session["email"] = "example@gmail.com"
        session.save()
        donor = Donor(username = "name", email = "example@gmail.com")
        donor.save()
        program = Program()
        program.name = "Program 1"
        program.save()

    def test_donating_url_with_session(self):
        found = self.client.get("/programs/program-1/donate/")
        self.assertEqual(found.status_code, 200)

    def test_donating_function_with_session(self):
        found = resolve("/programs/program-1/donate/")
        self.assertEqual(found.func, program_donate)

    def test_donation_form_valid(self):
        data = { 'anon' : False, 'amount' : 2000 }
        response = self.client.post('/programs/program-1/donate/', data=json.dumps(data), content_type='application/json')
        json_response = response.json()
        self.assertEqual(json_response["status"], 1)

    def test_donation_form_invalid(self):
        data = { 'anon' : None, 'amount' : None }
        response = self.client.post('/programs/program-1/donate/', data=json.dumps(data), content_type='application/json')
        json_response = response.json()
        self.assertEqual(json_response["status"], 0)

class DonationViewWithoutSession(TestCase):
    def setUp(self):
        donor = Donor(username = "name", email = "example@gmail.com")
        donor.save()
        program = Program()
        program.name = "Program 1"
        program.save()
    
    def test_donation_form_without_session(self):
        found = self.client.get("/programs/program-1/donate/")
        self.assertEqual(found.status_code, 302)