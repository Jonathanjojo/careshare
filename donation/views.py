from django.shortcuts import render, redirect
from .forms import DonationForm
from .models import Donation
from regisform.models import Donor
from program.models import Program
from django.http import JsonResponse
from django.urls import reverse
import json

def program_donate(request, slug):
    if "user_id" in request.session:
        program = Program.objects.get(available=True, slug=slug)
        response = {
            "page_title": "Donation Page",
            "navs": True,
            'footer': 'CareShare, est.2018',
            'form': DonationForm(),
            'program' : program
        }
        if request.method == "POST":
            data = json.loads(request.body)
            form = DonationForm({
                'anon' : data['anon'],
                'amount' : data['amount']
            })
            if form.is_valid():
                donation = Donation()
                donation.program = program
                if 0 < form.cleaned_data["amount"] < 9999999999999:
                    donation.amount = form.cleaned_data["amount"]
                    donation.anon = form.cleaned_data["anon"]
                    donation.donor = Donor.objects.get(
                        username = request.session['name'],
                        email = request.session['email'],
                    )
                    donation.save()
                    return JsonResponse(
                        {
                            "status" : 1,
                            "feedback" : "",
                            "url" : reverse("program:program_details" , kwargs={'slug':slug} )
                        }
                    )
            return JsonResponse(
                {
                    "status" : 0,
                    "feedback" : "Your entry is not valid"
                }
            )
        else:
            return render(request, 'donation/donation.html', response)
    return redirect("news:news_page")