function createDonation(){
    event.preventDefault(true);
    let fields = document.forms["donation-form"].getElementsByTagName("input");
    let csrf_token_entry = fields[0].value;
    let anon_entry = fields[1].checked;
    let amount_entry = fields[2].value;
    $.ajax({
        url: '',
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify({
            anon : anon_entry,
            amount : amount_entry,
        }),
        beforeSend: function(request) {
            request.setRequestHeader('X-CSRFToken', csrf_token_entry);
        },
        success: function(result) {
            let feedback = document.getElementById("feedback");
            let button = document.getElementById("submit-button");
            if(result.status == 1){
                feedback.innerText = '';
                window.location.replace(result.url);
            } else {
                feedback.innerText = result.feedback;
            }
        }
    });
}