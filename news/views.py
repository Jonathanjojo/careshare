from django.shortcuts import render
from .models import *

# Create your views here.

response = {}
def news_page(request):
    news_list = News.objects.all()
    response = {
        "page_title": "News Page",
        "news_list" : news_list,
        "navs" : True,
        'footer' : 'CareShare, est.2018'
    }
    return render(request, 'news.html', response)