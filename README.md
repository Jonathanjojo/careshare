# Crowd-Funding Web Project - PPW B - F 

[![build status](https://gitlab.com/Tugas-1-PPW-B5/crowd-funding/badges/master/build.svg)](https://gitlab.com/Tugas-1-PPW-B5/crowd-funding/commits/master) [![coverage report](https://gitlab.com/Tugas-1-PPW-B5/crowd-funding/badges/master/coverage.svg)](https://gitlab.com/Tugas-1-PPW-B5/crowd-funding/commits/master)


## URL

This project can be accessed from [https://crowd-funding-ppw-bf.herokuapp.com](https://crowd-funding-ppw-bf.herokuapp.com)

## Authors

* **Aditya Nanda Tri Prakoso** - [AdityaNandaaa](https://gitlab.com/adityanandaaa)
* **Dafa Ramadansyah** - [Darrand](https://gitlab.com/Darrand)
* **Jonathan Christopher Jakub** - [Jonathanjojo](https://gitlab.com/Jonathanjojo)
* **Palito** - [PalitoJeremy](https://gitlab.com/palitojeremy)


## Authors Contributions

* **Aditya Nanda Tri Prakoso** - [AdityaNandaaa](https://gitlab.com/adityanandaaa)
* **Dafa Ramadansyah** - [Darrand](https://gitlab.com/Darrand)
    * Designed 4 out of 8 wireframe pages.
    * Designed 1 high-fidelity page and recreated 1 high-fidelity page on marvelapp
    * created and mantained the news app fully with TDD 
    * provided support and help to the team with backend related content
    * provided support in debugging the web 
    * provided a fix responsive design on all the forms html
    * provided a fix password confirmation for donating 
    * provided a fix for alert whenever user is logged in
    * Added tests for donation

* **Jonathan Christopher Jakub** - [Jonathanjojo](https://gitlab.com/Jonathanjojo)
    * Initiated develop-ready group and project, full with its CI/CD implementation and Heroku prerequisites fulfilled. Successfuly delivered the landing page by the first stage deadline.
    * Created users' personas and stories.
    * Co-designed the early graphical prototypes on wireframe and personally designed 4 out of 8 available pages. Initially, proposed the color theme which then was approved and used for this project.
    * Co-designed, yet personally created the high-fidelity prototype for 5 out of 8 total pages in which 3 of the 5 pages were assigned to my responsibility: the landing page, the program list page, and the program details page.
    * Co-designed the models infrastructure and relationships for the whole project.
    * Provided the base html layouts, stylesheets, and several static files which then were used by all the apps in this project.
    * Fully responsible for the landing-page app and the programs app. Ocassionaly handed supports for other apps development.
    * Personally completed the requirements for handed apps which I am responsible for: landing page and programs app with TDD implementation. During the process, the landing_page and the program app were made sure to reach 100% code coverage. While working on the program app, I also provided a tested donation model.
    * Landing page was made minimalistic, straight forward, and living up to the theme.
    * Made programs list page in reference to the theme and kept it simplistic yet serious, giving a calming and assuring vibes to expected users. This page does the job of delivering compact information about programs successfully.
    * Designed programs models to be closed for modification, but opened to extension. Their attributes are able to sustain its existance with no conflicts nor side effects, even though a program refers to a news in a one-to-many relationship. Program details page of each program can be accessed by typing the slug of the program title, which in the future can open-up to auto-slugifying feature if there would be a program creation page.
    * Populated the database with usable contents.
    * Provided support to the team and ocasionally give consultion in trouble-shooting.
    * Single handedly created all the auth features
    * Rework previous works in order to adapt to upcoming auth features.
    * Revamped views.

* **Palito** - [PalitoJeremy](https://gitlab.com/palitojeremy)

## Acknowledgments

* PPW CSUI 2018
* Lecturer : Bu Maya Retno
* Unmentioned teaching assistants